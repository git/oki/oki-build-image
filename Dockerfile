FROM gcc:14
RUN apt-get update \
    && apt-get -y --no-install-recommends install nlohmann-json3-dev libcurl4-openssl-dev libminizip-dev libboost-program-options-dev doctest-dev gcovr \
    && wget -q 'https://raw.githubusercontent.com/marzer/tomlplusplus/v3.4.0/toml.hpp' -O /usr/local/include/toml.hpp
